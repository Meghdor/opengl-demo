#include <glm/gtc/constants.hpp>

#include "Primitive.hpp"

int Primitive::loadResource(const std::string& resourceFile)
{
	int returnCode = 0;

	fprintf(stderr, "Loading %s.\n", resourceFile.c_str());
	if(resourceFile == "sphere")
	{
		returnCode = loadSphere(40, 40);
	}
	else if(resourceFile == "cube")
	{
		returnCode = loadCube();
	}
	else if(resourceFile == "skybox")
	{
		returnCode = loadSkybox();
	}

	return returnCode;
}

int Primitive::loadSkybox()
{
	vertices.push_back(glm::vec3(1.0f, 1.0f, -1.0f));
	vertices.push_back(glm::vec3(1.0f, -1.0f, -1.0f));
	vertices.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
	vertices.push_back(glm::vec3(1.0f, -1.0f, 1.0f));

	vertices.push_back(glm::vec3(-1.0f, 1.0f, 1.0f));
	vertices.push_back(glm::vec3(-1.0f, -1.0f, 1.0f));
	vertices.push_back(glm::vec3(-1.0f, 1.0f, -1.0f));
	vertices.push_back(glm::vec3(-1.0f, -1.0f, -1.0f));

	vertices.push_back(glm::vec3(-1.0f, -1.0f, -1.0f));
	vertices.push_back(glm::vec3(-1.0f, -1.0f, 1.0f));
	vertices.push_back(glm::vec3(1.0f, -1.0f, -1.0f));
	vertices.push_back(glm::vec3(1.0f, -1.0f, 1.0f));

	vertices.push_back(glm::vec3(-1.0f, 1.0f, 1.0f));
	vertices.push_back(glm::vec3(-1.0f, 1.0f, -1.0f));
	vertices.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
	vertices.push_back(glm::vec3(1.0f, 1.0f, -1.0f));

	vertices.push_back(glm::vec3(-1.0f, 1.0f, -1.0f));
	vertices.push_back(glm::vec3(-1.0f, -1.0f, -1.0f));
	vertices.push_back(glm::vec3(1.0f, 1.0f, -1.0f));
	vertices.push_back(glm::vec3(1.0f, -1.0f, -1.0f));

	vertices.push_back(glm::vec3(1.0f, 1.0f, 1.0f));
	vertices.push_back(glm::vec3(1.0f, -1.0f, 1.0f));
	vertices.push_back(glm::vec3(-1.0f, 1.0f, 1.0f));
	vertices.push_back(glm::vec3(-1.0f, -1.0f, 1.0f));

	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	return prepareBuffers(false);
}

int Primitive::loadCube()
{
	vertices.push_back(glm::vec3(-0.5, -0.5, -0.5));
	vertices.push_back(glm::vec3(-0.5, 0.5, -0.5));
	vertices.push_back(glm::vec3(0.5, 0.5, -0.5));
	vertices.push_back(glm::vec3(0.5, -0.5, -0.5));

	vertices.push_back(glm::vec3(0.5, -0.5, 0.5));
	vertices.push_back(glm::vec3(0.5, 0.5, 0.5));
	vertices.push_back(glm::vec3(-0.5, 0.5, 0.5));
	vertices.push_back(glm::vec3(-0.5, -0.5, 0.5));

	vertices.push_back(glm::vec3(0.5, -0.5, -0.5));
	vertices.push_back(glm::vec3(0.5, 0.5, -0.5));
	vertices.push_back(glm::vec3(0.5, 0.5, 0.5));
	vertices.push_back(glm::vec3(0.5, 0.5, 0.5));

	vertices.push_back(glm::vec3(-0.5, -0.5, 0.5));
	vertices.push_back(glm::vec3(-0.5, 0.5, 0.5));
	vertices.push_back(glm::vec3(-0.5, 0.5, -0.5));
	vertices.push_back(glm::vec3(-0.5, -0.5, -0.5));

	vertices.push_back(glm::vec3(0.5, 0.5, 0.5));
	vertices.push_back(glm::vec3(0.5, 0.5, -0.5));
	vertices.push_back(glm::vec3(-0.5, 0.5, -0.5));
	vertices.push_back(glm::vec3(-0.5, 0.5, 0.5));

	vertices.push_back(glm::vec3(0.5, -0.5, -0.5));
	vertices.push_back(glm::vec3(0.5, -0.5, 0.5));
	vertices.push_back(glm::vec3(-0.5, -0.5, 0.5));
	vertices.push_back(glm::vec3(-0.5, -0.5, -0.5));

	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	uvs.push_back(glm::vec2(0.0f, 0.0f));
	uvs.push_back(glm::vec2(0.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 1.0f));
	uvs.push_back(glm::vec2(1.0f, 0.0f));

	normals.push_back(glm::normalize(glm::vec3(-0.5, -0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, 0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, -0.5, -0.5)));

	normals.push_back(glm::normalize(glm::vec3(0.5, -0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, 0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, -0.5, 0.5)));

	normals.push_back(glm::normalize(glm::vec3(0.5, -0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, 0.5)));

	normals.push_back(glm::normalize(glm::vec3(-0.5, -0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, 0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, 0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, -0.5, -0.5)));

	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, 0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, 0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, 0.5, 0.5)));

	normals.push_back(glm::normalize(glm::vec3(0.5, -0.5, -0.5)));
	normals.push_back(glm::normalize(glm::vec3(0.5, -0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, -0.5, 0.5)));
	normals.push_back(glm::normalize(glm::vec3(-0.5, -0.5, -0.5)));

	return prepareBuffers();
}

int Primitive::loadSphere(int rings, int sectors)
{
	float const R = (float)1. / (float)(rings - 1);
	float const S = (float)1. / (float)(sectors - 1);
	float pi2 = glm::half_pi<float>();
	float pi = glm::pi<float>();
	float radius = 1.0f;


	vertices.reserve((rings - 1) * (sectors - 1) * 6);
	normals.reserve((rings - 1) * (sectors - 1) * 6);
	uvs.reserve((rings - 1) * (sectors - 1) * 6);
	std::vector<glm::vec3> tempVect;
	std::vector<glm::vec2> tempUV;

	for(int r = 0; r < rings; ++r)
	{
		for(int s = 0; s < sectors; ++s)
		{
			float const y = radius * -cos(pi * r * R); //sin(-pi2 + pi * r * R); 
			float const x = radius * cos(2 * pi * s * S) * sin(pi * r * R);
			float const z = radius * sin(2 * pi * s * S) * sin(pi * r * R);

			tempUV.push_back(glm::vec2(s*S, r*R));
			tempVect.push_back(glm::vec3(x, y, z));
			//normals.push_back(glm::vec3(x, y, z));
		}
	}

	for(int r = 0; r < rings - 1; ++r)
	{
		for(int s = 0; s < sectors - 1; ++s)
		{
			int curRow = r * sectors;
			int nextRow = (r + 1) * sectors;
			vertices.push_back(tempVect[curRow + s]);
			vertices.push_back(tempVect[nextRow + s]);
			vertices.push_back(tempVect[nextRow + (s + 1)]);

			vertices.push_back(tempVect[curRow + s]);
			vertices.push_back(tempVect[nextRow + (s + 1)]);
			vertices.push_back(tempVect[curRow + (s + 1)]);

			normals.push_back(glm::normalize(tempVect[curRow + s]));
			normals.push_back(glm::normalize(tempVect[nextRow + s]));
			normals.push_back(glm::normalize(tempVect[nextRow + (s + 1)]));

			normals.push_back(glm::normalize(tempVect[curRow + s]));
			normals.push_back(glm::normalize(tempVect[nextRow + (s + 1)]));
			normals.push_back(glm::normalize(tempVect[curRow + (s + 1)]));

			uvs.push_back(tempUV[curRow + s]);
			uvs.push_back(tempUV[nextRow + s]);
			uvs.push_back(tempUV[nextRow + (s + 1)]);

			uvs.push_back(tempUV[curRow + s]);
			uvs.push_back(tempUV[nextRow + (s + 1)]);
			uvs.push_back(tempUV[curRow + (s + 1)]);
		}
	}

	return prepareBuffers();
}
