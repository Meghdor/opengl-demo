#include <iostream>

#include "Mesh.hpp"
#include "Primitive.hpp"
#include "Resource.hpp"
#include "Shader.hpp"
#include "Texture.hpp"

extern ResourceManager* g_resourceManager = 0;

Resource* ResourceManager::findResource(ResourceType resourceType, const std::string& resourceName)
{
	std::string resourceMapName = typePrefixes[resourceType] + resourceName;
	ResourceMapType::iterator returnResource = resourceMap.find(resourceMapName);

	if(returnResource != resourceMap.end())
	{
		return (*returnResource).second;
	}
	
	Resource* newResource;

	switch(resourceType)
	{
	case ResourceType::LEVEL:
		break;
	case ResourceType::MESH:
		newResource = new Mesh();
		break;
	case ResourceType::MUSIC:
		break;
	case ResourceType::PRIMITIVE:
		newResource = new Primitive();
		break;
	case ResourceType::SHADER:
		newResource = new Shader();
		break;
	case ResourceType::TEXTURE:
		newResource = new Texture();
		break;
	}

	int returnCode = newResource->loadResource(resourceName);
	if(returnCode != 0)
	{
		fprintf(stderr, "Can't load resource %s. Error %d\n", resourceMapName.c_str(), returnCode);
		delete newResource;
		return NULL;
	}

	resourceMap.insert(ResourceMapType::value_type(resourceMapName, newResource));
	return newResource;
}

void ResourceManager::destroy()
{
	for(ResourceMapType::iterator i = resourceMap.begin(); i != resourceMap.end();)
	{
		delete (*i).second;
		i = resourceMap.erase(i);
	}
}
