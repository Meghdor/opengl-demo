#include <gl/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "GameplayCamera.hpp"

void GameplayCamera::update(float deltaTime)
{
	glm::vec3 right = glm::cross(direction, up);

	if(rotationVelocity.y)
	{
		direction = glm::vec3(glm::rotate(glm::mat4(1.0f), rotationVelocity.y, right)*glm::vec4(direction, 0.0f));
		up = glm::cross(right, direction);
	}
	if(rotationVelocity.z)
	{
		up = glm::vec3(glm::rotate(glm::mat4(1.0f), rotationVelocity.z, direction)*glm::vec4(up, 0.0f));
		right = glm::cross(direction, up);
	}
	// @TODO Do something about "double-crossing" to get the up vector, 'couse that's just silly now.
	if(rotationVelocity.x)
	{
		direction = glm::vec3(glm::rotate(glm::mat4(1.0f), rotationVelocity.x, up)*glm::vec4(direction, 0.0f));
		right = glm::cross(direction, up);
	}

	position += velocity * deltaTime;

	viewMatrix = glm::lookAt(position, position + direction, up);
	projectionMatrix = glm::perspective(fieldOfView, aspectRatio, 0.1f, 1000.0f);
}

void GameplayCamera::changeVelocity(glm::vec3 velocityDelta)
{
	velocity += velocityDelta * speed;
}

void GameplayCamera::changeRotationVelocity(glm::vec3 rotationVelocityDelta)
{
	rotationVelocity += rotationVelocityDelta * rotationSpeed;
}

