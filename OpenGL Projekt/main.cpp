#include <cstdio>
#include <cstdlib>
#include <GL/glew.h>

#include "Game.hpp"

int main(void)
{

	Game game;
	if(game.init() == 0)
	{
		game.run();
	}
	
	return 0;
}

