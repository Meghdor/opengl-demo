#include <cstdio>
#include <cstdlib>

#include <gl/glew.h>
#include <gl/glfw3.h>

#include "Game.hpp"
#include "GameScreen.hpp"
#include "Resource.hpp"

int Game::init()
{
	if(!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(800, 600, "OpenGL Demo", NULL, NULL);

	if(!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}

	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(window, Game::key_callback);
	glfwSetFramebufferSizeCallback(window, Game::framebufferSizeCallback);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glEnable(GL_CULL_FACE);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	g_resourceManager = new ResourceManager();

	currentScreen = new GameScreen(window);
	if(currentScreen->init())
	{
		return 1;
	}

	return 0;
}

int Game::run()
{
	do
	{
		static double lastTime = 0;
		static double fpsLastTime = 0;
		double timeNow = glfwGetTime();
		static short FPScount = 0;

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		currentScreen->update((float)(timeNow - lastTime));
		currentScreen->render();

		++FPScount;

		if(timeNow - fpsLastTime >= 1)
		{
			char tytul[16];
			sprintf_s(tytul, "FPS: %hd", FPScount);
			glfwSetWindowTitle(window, tytul);
			FPScount = 0;
			fpsLastTime = timeNow;
		}
		
		lastTime = timeNow;

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	while(!glfwWindowShouldClose(window));

	return 0;
}

void Game::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

int Game::destroy()
{
	currentScreen->destroy();
	delete g_resourceManager;
	glfwDestroyWindow(window);
	glfwTerminate();
	
	return 0;
}

void Game::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}
