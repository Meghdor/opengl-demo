#ifndef INPUT_HPP_INC
#define INPUT_HPP_INC

#include <GL/glew.h>
#include <GL/glfw3.h>

class Input
{
protected:
	GLFWwindow* inputWindow;

public:
	Input(GLFWwindow* window): inputWindow(window) { }
	virtual void update(float deltaTime) = 0;
	void setInputWindow(GLFWwindow* window)
	{
		inputWindow = window;
	}
};

#endif
