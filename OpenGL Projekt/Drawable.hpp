#ifndef DRAWABLE_HPP_INC
#define DRAWABLE_HPP_INC

class Drawable
{
public:
	virtual void render() = 0;
	virtual void update(float delta) = 0;
};

#endif
