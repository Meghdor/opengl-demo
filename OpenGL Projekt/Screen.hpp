#ifndef SCREEN_HPP_INC
#define SCREEN_HPP_INC

#include <GL/glfw3.h>
#include <glm/glm.hpp>

class Screen
{
protected:
	GLFWwindow * window;

public:
	Screen(GLFWwindow * screenWindow): window(screenWindow) { }
	virtual int init() = 0;
	virtual void render() = 0;
	virtual void update(float deltaTime) = 0;
	virtual int destroy() = 0;
};

#endif
