#include <glm/gtc/matrix_transform.hpp>

#include "GameInput.hpp"

void GameInput::update(float deltaTime)
{
	glm::vec3 direction = gameCamera->getCameraDirection();
	glm::vec3 up = gameCamera->getCameraUpVector();
	glm::vec3 right = glm::cross(direction, up);
	
	glm::vec3 velocityDelta = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 rotationVelocityDelta = glm::vec3(0.0f, 0.0f, 0.0f);

	// Move forward
	if(glfwGetKey(inputWindow, GLFW_KEY_W) == GLFW_PRESS)
	{
		velocityDelta += direction * deltaTime;
	}
	// Move backward
	if(glfwGetKey(inputWindow, GLFW_KEY_S) == GLFW_PRESS)
	{
		velocityDelta -= direction * deltaTime;
	}
	// Strafe right
	if(glfwGetKey(inputWindow, GLFW_KEY_D) == GLFW_PRESS)
	{
		velocityDelta += right * deltaTime;
	}
	// Strafe left
	if(glfwGetKey(inputWindow, GLFW_KEY_A) == GLFW_PRESS)
	{
		velocityDelta -= right * deltaTime;
	}
	// Turn up
	if(glfwGetKey(inputWindow, GLFW_KEY_UP) == GLFW_PRESS)
	{
		rotationVelocityDelta.y -= deltaTime;
	}
	// Turn down
	if(glfwGetKey(inputWindow, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		rotationVelocityDelta.y += deltaTime;
	}
	// Turn right
	if(glfwGetKey(inputWindow, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		rotationVelocityDelta.x -= deltaTime;
	}
	// Turn left
	if(glfwGetKey(inputWindow, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		rotationVelocityDelta.x += deltaTime;
	}
	// Spin right
	if(glfwGetKey(inputWindow, GLFW_KEY_Q) == GLFW_PRESS)
	{
		rotationVelocityDelta.z -= deltaTime;
	}
	// Spin left
	if(glfwGetKey(inputWindow, GLFW_KEY_E) == GLFW_PRESS)
	{
		rotationVelocityDelta.z += deltaTime;
	}
	// Move up
	if(glfwGetKey(inputWindow, GLFW_KEY_SPACE) == GLFW_PRESS)
	{
		velocityDelta += up * deltaTime;
	}
	// Move down
	if(glfwGetKey(inputWindow, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS)
	{
		velocityDelta -= up * deltaTime;
	}

	gameCamera->changeRotationVelocity(rotationVelocityDelta);
	gameCamera->changeVelocity(velocityDelta);
}