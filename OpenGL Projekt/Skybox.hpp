#ifndef SKYBOX_HPP_INC
#define SKYBOX_HPP_INC

#include "DrawableObject.hpp"

class Skybox: public DrawableObject
{
	Texture* skyTexture[6];
public:
	Skybox() { }
	~Skybox()
	{
		destroy();
	}
	int init();
	void render();
	void destroy();
};

#endif
