#ifndef PRIMITIVE_HPP_INC
#define PRIMITIVE_HPP_INC

#include "Mesh.hpp"

class Primitive: public Mesh
{
public:
	int loadResource(const std::string& resourceFile);
	int loadCube();
	int loadSphere(int rings, int sectors);
	int loadSkybox();
};

#endif

