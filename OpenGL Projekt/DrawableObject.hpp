#ifndef THREEDOBJECT_HPP_INC
#define THREEDOBJECT_HPP_INC

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "Camera.hpp"
#include "Drawable.hpp"
#include "Mesh.hpp"
#include "Shader.hpp"
#include "Texture.hpp"


class DrawableObject: public Drawable
{
public:
	enum MeshSource {MESH_PRIMITIVE, MESH_FILE};
protected:
	Shader* shader;
	Mesh* mesh;
	Texture* texture;
	glm::mat4 modelMatrix;
	
public:
	DrawableObject(): shader(NULL), mesh(NULL), texture(NULL), modelMatrix(glm::mat4(1.0f)) { }
	~DrawableObject()
	{
		destroy();
	}
	virtual int init(MeshSource meshSource, const std::string& meshName, const std::string& textureName, const std::string& shader);
	virtual void render();
	virtual void update(float deltaTime);
	virtual void destroy();

	void setModelMatrix(const glm::mat4 modelMatrix);
	void setShader(Shader* newShader)
	{
		shader = newShader;
	}
};

#endif
