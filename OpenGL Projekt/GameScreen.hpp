#ifndef GAMESCREEN_HPP_INC
#define GAMESCREEN_HPP_INC

#include <GL/glew.h>
#include <GL/glfw3.h>

#include "Screen.hpp"
#include "Camera.hpp"
#include "DrawableObject.hpp"
#include "GameInput.hpp"

class GameScreen: public Screen
{
	Camera* gameCamera;
	GameInput* gameInput;
	std::vector<DrawableObject*> sceneObjects;
	std::vector<Shader*> shaders;
public:
	GameScreen(GLFWwindow * screenWindow): gameCamera(NULL), gameInput(NULL), Screen(screenWindow) { }
	~GameScreen()
	{
		destroy();
	}
	int init();
	void render();
	void update(float deltaTime);
	int destroy();
};

#endif
