#ifndef TEXTURE_HPP_INC
#define TEXTURE_HPP_INC

#define FOURCC_DXT1 0x31545844 // Equivalent to "DXT1" in ASCII
#define FOURCC_DXT3 0x33545844 // Equivalent to "DXT3" in ASCII
#define FOURCC_DXT5 0x35545844 // Equivalent to "DXT5" in ASCII

#include <GL/glew.h>

#include "Resource.hpp"

class Texture: public Resource
{
protected:
	GLuint textureHandler;
	GLuint textureSampler;

public:
	Texture(): textureHandler(0), textureSampler(0) { }
	int loadResource(const std::string& resourceFile);
	int loadDDS(const char* fileName);
	void bindTexture();
	void setSamplerParam(GLenum param, GLenum val);
	~Texture()
	{
		glDeleteTextures(1, &textureHandler);
	}
};

#endif
