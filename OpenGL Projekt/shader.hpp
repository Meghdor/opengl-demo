#ifndef SHADER_HPP_INC
#define SHADER_HPP_INC

#include <glm/gtc/type_ptr.hpp>
#include "Camera.hpp"
#include "Resource.hpp"

class Shader: public Resource
{
private:
	enum ShaderType {VERTEX, FRAGMENT, GEOMETRY};
	const char* shaderTypeText[3] = {"vertex", "fragment", "geometry"};

	GLuint programID;
	std::vector<GLuint> uniforms;
	glm::vec3 lightDir;
	Camera* camera;

public:
	Shader(): programID(0), camera(NULL), lightDir(glm::vec3(0.0f, 0.0f, 1.0f)) { }
	~Shader()
	{
		destroy();
	}
	void destroy();
	GLuint loadShader(Shader::ShaderType shaderType, const std::string& shaderName);
	int loadResource(const std::string& resourceFile);
	Camera* getCamera()
	{
		return camera;
	}
	void setCamera(Camera* argCamera)
	{
		camera = argCamera;
	}
	void update();
	void update(glm::mat4& modelMatrix);
	void setUniform(int uniform, const glm::mat4& matrix);
	void setUniform(int uniform, const glm::vec3& vector);
	void setUniform(int uniform, GLint number);
	void useProgram();
};

#endif
