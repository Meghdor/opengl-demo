#ifndef GAMEPLAYCAMERA_HPP_INC
#define GAMEPLAYCAMERA_HPP_INC

#include "Camera.hpp"

/**
 *
 */
class GameplayCamera : public Camera
{
protected:
	glm::vec3 velocity;
	glm::vec3 rotationVelocity;
	float speed;
	float rotationSpeed;
	float mouseSpeed;

public:
	GameplayCamera(glm::vec3 cameraPosition, glm::vec3 cameraDirection, glm::vec3 upVector): Camera(cameraPosition, cameraDirection, upVector), velocity(glm::vec3(0.0f, 0.0f, 0.0f)), rotationVelocity(glm::vec3(0.0f, 0.0f, 0.0f)), speed(8.0f), rotationSpeed(4.0f), mouseSpeed(0.05f) { }
	void update(float deltaTime);
	glm::vec3 getCameraVelocity()
	{
		return velocity;
	}
	void changeVelocity(glm::vec3 velocityDelta);
	void changeRotationVelocity(glm::vec3 rotationVelocityDelta);
};

#endif

