#include "DrawableObject.hpp"

int DrawableObject::init(MeshSource meshSource, const std::string& meshName, const std::string& textureName, const std::string& shader)
{
	this->shader = (Shader*)g_resourceManager->findResource(ResourceManager::SHADER, shader);

	if(meshSource == MESH_FILE)
	{
		mesh = (Mesh*)g_resourceManager->findResource(ResourceManager::MESH, meshName);
	}
	else
	{
		mesh = (Mesh*)g_resourceManager->findResource(ResourceManager::PRIMITIVE, meshName);
	}
	
	texture = (Texture*)g_resourceManager->findResource(ResourceManager::TEXTURE, textureName);

	return mesh == NULL || texture == NULL;
}

void DrawableObject::render()
{
	glBindVertexArray(mesh->getVAO());
	
	shader->useProgram();
	shader->update(modelMatrix);
	shader->setUniform(3, 0);

	texture->bindTexture();

	glDrawArrays(GL_TRIANGLES, 0, mesh->getVerticesSize());
}

void DrawableObject::update(float deltaTime)
{
	return;
}

void DrawableObject::destroy()
{
	shader = NULL;
	mesh = NULL;
	texture = NULL;
}

void DrawableObject::setModelMatrix(const glm::mat4 modelMatrix)
{
	this->modelMatrix = modelMatrix;
}
