#ifndef MESH_HPP_INC
#define MESH_HPP_INC

#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "Resource.hpp"

class Mesh: public Resource
{
protected:
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	GLuint vaoID;
	GLuint vertexBuffer;
	GLuint uvBuffer;
	GLuint normalBuffer;

public:
	Mesh(): vaoID(0), vertexBuffer(0), uvBuffer(0), normalBuffer(0) { }
	~Mesh();
	virtual int loadResource(const std::string& resourceFile);
	int prepareBuffers(bool normalBufferEnabled = true);
	bool loadObjFile(const char * path);
	GLuint getVAO() { return vaoID; }
	GLsizei getVerticesSize()
	{
		return (GLsizei)vertices.size();
	}
};

#endif

