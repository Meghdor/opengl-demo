#ifndef CAMERA_HPP_INC
#define CAMERA_HPP_INC

#include <GL/glew.h>
#include <GL/glfw3.h>

#include <glm/glm.hpp>

/**
	Camera class
*/
class Camera
{
protected:
	float aspectRatio;
	float fieldOfView;
	glm::vec3 direction;
	glm::vec3 position;
	glm::vec3 up;
	glm::mat4 viewMatrix;
	glm::mat4 projectionMatrix;

public:
	Camera(glm::vec3 cameraPosition, glm::vec3 cameraDirection, glm::vec3 upVector): aspectRatio(1.3f), fieldOfView(45.0f), position(cameraPosition), direction(glm::normalize(cameraDirection)), up(glm::normalize(upVector)) { }
	virtual void update(float deltaTime) = 0;
	glm::mat4& getViewMatrix() { return viewMatrix; }
	glm::mat4& getProjectionMatrix() { return projectionMatrix; }
	glm::vec3 getCameraPosition() { return position; }
	glm::vec3 getCameraDirection() { return direction; }
	glm::vec3 getCameraUpVector() { return up; }
	void setAspectRatio(float newAspect) { aspectRatio = newAspect; }
};

#endif
