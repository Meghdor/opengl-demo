#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "Texture.hpp"

int Texture::loadResource(const std::string& resourceFile)
{
	std::string fileName = "assets/texture/" + resourceFile + ".dds";
	fprintf(stderr, "Loading %s.\n", fileName.c_str());
	return loadDDS(fileName.c_str());
}

/**
 * Texture loading function
 * Borrowed from 
 */
int Texture::loadDDS(const char* imagepath)
{
	unsigned char header[124];

	FILE* fp; 
 
	/* try to open the file */ 
	fopen_s(&fp, imagepath, "rb"); 
	if(fp == NULL)
	{
		fprintf(stderr, "%s could not be opened.\n", imagepath);
		return 1;
	}
   
	/* verify the type of file */ 
	char filecode[4]; 
	fread(filecode, 1, 4, fp); 
	if (strncmp(filecode, "DDS ", 4) != 0)
	{ 
		fprintf(stderr, "%s not a DDS file.\n", imagepath);
		fclose(fp); 
		return 2; 
	}
	
	/* get the surface desc */ 
	fread(&header, 124, 1, fp); 

	unsigned int height      = *(unsigned int*)&(header[ 8]);
	unsigned int width	     = *(unsigned int*)&(header[12]);
	unsigned int linearSize	 = *(unsigned int*)&(header[16]);
	unsigned int mipMapCount = *(unsigned int*)&(header[24]);
	unsigned int fourCC      = *(unsigned int*)&(header[80]);
	 
	unsigned char* buffer;
	unsigned int bufsize;
	
	/* how big is it going to be including all mipmaps? */ 
	bufsize = mipMapCount > 1 ? linearSize * 2 : linearSize; 
	buffer = (unsigned char*)malloc(bufsize * sizeof(unsigned char)); 
	fread(buffer, 1, bufsize, fp); 
	/* close the file pointer */ 
	fclose(fp);

	unsigned int components = (fourCC == FOURCC_DXT1) ? 3 : 4; 
	unsigned int format;
	switch(fourCC) 
	{ 
	case FOURCC_DXT1: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT; 
		break; 
	case FOURCC_DXT3: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT; 
		break; 
	case FOURCC_DXT5: 
		format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT; 
		break; 
	default: 
		free(buffer); 
		return 0; 
	}

	// Create one OpenGL texture
	GLuint textureID;
	glGenTextures(1, &textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, textureID);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	
	unsigned int blockSize = (format == GL_COMPRESSED_RGBA_S3TC_DXT1_EXT) ? 8 : 16; 
	unsigned int offset = 0;

	/* load the mipmaps */ 
	for(unsigned int level = 0; level < mipMapCount && (width || height); ++level) 
	{ 
		unsigned int size = ((width+3)/4) * ((height+3)/4) * blockSize; 
		glCompressedTexImage2D(GL_TEXTURE_2D, level, format, width, height, 0, size, buffer + offset); 
	 
		offset += size; 
		width  /= 2; 
		height /= 2; 

		// Deal with Non-Power-Of-Two textures. This code is not included in the webpage to reduce clutter.
		if(width < 1) width = 1;
		if(height < 1) height = 1;
	} 

	free(buffer);
	textureHandler = textureID;

	glGenSamplers(1, &textureSampler);

	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, textureID);

	return 0;
}

void Texture::bindTexture()
{
	glBindTexture(GL_TEXTURE_2D, textureHandler);
	glBindSampler(0, textureSampler);
}

void Texture::setSamplerParam(GLenum param, GLenum val)
{
	glSamplerParameteri(textureSampler, param, val);
}
