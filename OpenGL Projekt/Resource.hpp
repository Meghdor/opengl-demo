#ifndef RESOURCE_HPP_INC
#define RESOURCE_HPP_INC

#include <unordered_map>

class Resource
{
public:
	Resource() { }
	~Resource() { }
	virtual int loadResource(const std::string& resourceFile) = 0;
};

/**
 * Resource manager
 * @TODO Cleanup
 */
class ResourceManager
{
private:
	typedef std::unordered_map<std::string, Resource*> ResourceMapType;
	ResourceMapType resourceMap;
	const char* typePrefixes[6] = {"level", "mesh", "music", "primitive", "shader", "texture"};

public:
	enum ResourceType {LEVEL, MESH, MUSIC, PRIMITIVE, SHADER, TEXTURE};

public:
	ResourceManager() { }
	~ResourceManager()
	{
		destroy();
	}
	void destroy();
	Resource* findResource(ResourceType resourceType, const std::string& resourceName);
};

extern ResourceManager* g_resourceManager;

#endif
