#ifndef GAME_HPP_INC
#define GAME_HPP_INC

#include <GL/glew.h>
#include <GL/glfw3.h>

#include "Screen.hpp"

class Game
{
private:
	GLFWwindow * window;
	Screen * currentScreen;
public:
	Game(): window(NULL), currentScreen(NULL) { }
	~Game()
	{
		destroy();
	}
	int init();
	int run();
	static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
	int destroy();
	static void framebufferSizeCallback(GLFWwindow* window, int width, int height);
};

#endif
