#ifndef GAMEINPUT_HPP_INC
#define GAMEINPUT_HPP_INC

#include "Input.hpp"
#include "GameplayCamera.hpp"

class GameInput: public Input
{
private:
	GameplayCamera* gameCamera;

public:
	GameInput(GLFWwindow* window, GameplayCamera* camera): Input(window), gameCamera(camera) { }
	void update(float deltaTime);
};

#endif

