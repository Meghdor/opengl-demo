#include "Skybox.hpp"

int Skybox::init()
{
	char * imagefiles[6] = {"skybox_right1", "skybox_left2", "skybox_top3", "skybox_bottom4", "skybox_front5", "skybox_back6"};
	int returnCode = 0;

	this->shader = (Shader*)g_resourceManager->findResource(ResourceManager::SHADER, "skybox");
	mesh = (Mesh*)g_resourceManager->findResource(ResourceManager::PRIMITIVE, "skybox");
	//glDisableVertexAttribArray(2);
	// @TODO Make into one texture
	for(int i = 0; i < 6; ++i)
	{
		skyTexture[i] = (Texture*)g_resourceManager->findResource(ResourceManager::TEXTURE, imagefiles[i]);
		skyTexture[i]->setSamplerParam(GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		skyTexture[i]->setSamplerParam(GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		++returnCode;
	}
	modelMatrix = glm::scale(glm::mat4(1.0f), glm::vec3(150.0f));

	return returnCode != 6 || mesh == NULL || shader == NULL;
}

void Skybox::render()
{
	glBindVertexArray(mesh->getVAO());
	
	shader->useProgram();

	glm::mat4 model = glm::translate(glm::mat4(1.0f), (shader->getCamera()->getCameraPosition())) * modelMatrix;
	shader->update(model);
	shader->setUniform(3, 0);

	glDepthMask(0);
	for(int i = 0; i < 6; ++i)
	{		
		skyTexture[i]->bindTexture();
		glDrawArrays(GL_TRIANGLE_STRIP, i*4, 4);
	}
	glDepthMask(1);
}

void Skybox::destroy()
{
	for(int i = 0; i < 6; ++i)
	{
		skyTexture[i] = NULL;
	}
}
