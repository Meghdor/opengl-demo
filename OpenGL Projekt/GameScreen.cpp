﻿#include "GameplayCamera.hpp"
#include "GameScreen.hpp"
#include "Shader.hpp"
#include "Skybox.hpp"


int GameScreen::init()
{
	shaders.push_back((Shader*)g_resourceManager->findResource(ResourceManager::SHADER, "scene_object"));
	if(shaders[0] == NULL)
	{
		return 1;
	}
	shaders.push_back((Shader*)g_resourceManager->findResource(ResourceManager::SHADER, "skybox"));
	if(shaders[1] == NULL)
	{
		return 1;
	}
	gameCamera = new GameplayCamera(glm::vec3(0.0f, 0.0f, 80.0f), glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, -1.0f, 0.0f));
	shaders[0]->setCamera(gameCamera);
	shaders[1]->setCamera(gameCamera);
	gameInput = new GameInput(window, (GameplayCamera*) gameCamera);
	sceneObjects.push_back(new Skybox());
	if(((Skybox*)(*sceneObjects.rbegin()))->init())
	{
		return 2;
	}
	
	DrawableObject* newSceneObject = new DrawableObject();
	if(newSceneObject->init(DrawableObject::MESH_PRIMITIVE, "sphere", "moon", "scene_object"))
	{
		return 3;
	}
	newSceneObject->setModelMatrix(glm::scale(glm::mat4(1.0f), glm::vec3(30.0f)));
	sceneObjects.push_back(newSceneObject);
	
	return 0;
}

void GameScreen::render()
{
	for(DrawableObject* object : sceneObjects)
	{
		object->render();
	}
}

void GameScreen::update(float deltaTime)
{
	int windowWidth, windowHeight;
	glfwGetWindowSize(window, &windowWidth, &windowHeight);
	gameInput->update(deltaTime);
	gameCamera->setAspectRatio((float)windowWidth / windowHeight);
	gameCamera->update(deltaTime);

	for(Shader* shader : shaders)
	{
		shader->update();
	}

	for(DrawableObject* object : sceneObjects)
	{
		object->update(deltaTime);
	}
}

int GameScreen::destroy()
{
	for(std::vector<Shader*>::iterator i = shaders.begin(); i != shaders.end(); )
	{
		i = shaders.erase(i);
	}

	for(std::vector<DrawableObject*>::iterator i = sceneObjects.begin(); i != sceneObjects.end(); )
	{
		delete (*i);
		i = sceneObjects.erase(i);
	}

	delete gameCamera;
	delete gameInput;

	return 0;
}
