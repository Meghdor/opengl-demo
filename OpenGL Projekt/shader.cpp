#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>

#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include "Shader.hpp"

int Shader::loadResource(const std::string& resourceFile)
{
	std::string fileName = "assets/shader/" + resourceFile;
	fprintf(stderr, "Loading %s.\n", fileName.c_str());
	
	GLuint VertexShaderID, FragmentShaderID;
	VertexShaderID = loadShader(VERTEX, resourceFile);
	FragmentShaderID = loadShader(FRAGMENT, resourceFile);

	GLint result = GL_FALSE;
	programID = glCreateProgram();
	glAttachShader(programID, VertexShaderID);
	glAttachShader(programID, FragmentShaderID);
	glLinkProgram(programID);
	glDeleteShader(VertexShaderID);
	glDeleteShader(FragmentShaderID);

	glGetProgramiv(programID, GL_LINK_STATUS, &result);

	if(result != GL_TRUE)
	{
		return 1;
	}

	uniforms.push_back(glGetUniformLocation(programID, "V"));
	uniforms.push_back(glGetUniformLocation(programID, "M"));
	uniforms.push_back(glGetUniformLocation(programID, "MVP"));
	uniforms.push_back(glGetUniformLocation(programID, "myTextureSampler"));
	uniforms.push_back(glGetUniformLocation(programID, "LightDirection_worldspace"));

	return 0;
}

GLuint Shader::loadShader(Shader::ShaderType shaderType, const std::string& shaderName)
{
	GLuint shaderID = 0;
	std::string shaderFile = "assets/shader/" + shaderName;

	switch(shaderType)
	{
	case VERTEX:
		shaderFile += ".v";
		shaderID = glCreateShader(GL_VERTEX_SHADER);
		break;
	case FRAGMENT:
		shaderFile += ".f";
		shaderID = glCreateShader(GL_FRAGMENT_SHADER);
		break;
	case GEOMETRY:
		shaderFile += ".g";
		shaderID = glCreateShader(GL_GEOMETRY_SHADER);
		break;
	default:
		return 0;
	}

	std::string shaderCode;
	std::ifstream shaderStream(shaderFile, std::ios::in);
	if(shaderStream.is_open())
	{
		std::string Line = "";
		while(getline(shaderStream, Line))
		{
			shaderCode += "\n" + Line;
		}
		shaderStream.close();
	}
	else
	{
		printf("Impossible to open %s.\n", shaderFile.c_str());
		return 0;
	}

	GLint result = GL_FALSE;

	const char* sourcePointer = shaderCode.c_str();
	glShaderSource(shaderID, 1, &sourcePointer, NULL);
	glCompileShader(shaderID);

	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &result);
	
	if(result == GL_TRUE)
	{
		return shaderID;
	}
	else
	{
		return 0;
	}
}

void Shader::update()
{
	glm::mat4 ProjectionMatrix = camera->getProjectionMatrix();
	glm::mat4 ViewMatrix = camera->getViewMatrix();

	setUniform(0, ViewMatrix);
	setUniform(4, lightDir);
	//glUniformMatrix4fv(uniforms[0], 1, GL_FALSE, glm::value_ptr(ViewMatrix));
	//glUniform3fv(uniforms[4], 1, glm::value_ptr(lightDir));
}

void Shader::update(glm::mat4& modelMatrix)
{
	glm::mat4 ProjectionMatrix = camera->getProjectionMatrix();
	glm::mat4 ViewMatrix = camera->getViewMatrix();
	glm::mat4 MVP = ProjectionMatrix * ViewMatrix * modelMatrix;
	
	setUniform(1, modelMatrix);
	setUniform(2, MVP);
}

void Shader::setUniform(int uniform, const glm::mat4& matrix)
{
	glUniformMatrix4fv(uniforms[uniform], 1, GL_FALSE, glm::value_ptr(matrix));
}

void Shader::setUniform(int uniform, const glm::vec3& vector)
{
	glUniform3fv(uniforms[uniform], 1, glm::value_ptr(vector));
}

void Shader::setUniform(int uniform, GLint number)
{
	glUniform1i(uniforms[uniform], number);
}

void Shader::useProgram()
{
	glUseProgram(programID);
}

void Shader::destroy()
{
	glDeleteProgram(programID);
	programID = 0;
	uniforms.erase(uniforms.begin(), uniforms.end());
	camera = NULL;
}
