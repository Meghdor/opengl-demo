#version 330 core

in vec3 outcolor;

out vec4 color;

void main()
{
	color.a = 1;
	color.rgb = outcolor;
}