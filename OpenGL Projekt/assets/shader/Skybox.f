#version 330 core

in vec2 UV;

out vec4 color;

uniform sampler2D myTextureSampler;

void main()
{
	color.a = 1;
	color.rgb = texture(myTextureSampler, UV).rgb;
}