#version 330 core

in vec2 UV;
in vec3 Position_worldspace;
in vec3 Normal_worldspace;
in vec3 EyeDirection_cameraspace;
in vec3 LightDirection_cameraspace;

out vec4 color;

uniform sampler2D myTextureSampler;
uniform mat4 MV;
uniform vec3 LightDirection_worldspace;

void main()
{
	vec3 LightColor = vec3(1, 1, 1);
	float LightPower = 1.0f;
	
	vec3 MaterialDiffuseColor = texture2D(myTextureSampler, UV).rgb;
	vec3 MaterialAmbientColor = vec3(0.1, 0.1, 0.1) * MaterialDiffuseColor;
	vec3 MaterialSpecularColor = vec3(0.3, 0.3, 0.3);
	
	vec3 n = normalize(Normal_worldspace);
	vec3 l = normalize(LightDirection_worldspace);
	
	float distance = 10;
	float cosTheta = clamp(dot(n, l), 0, 1);
	
	vec3 E = normalize(EyeDirection_cameraspace);
	vec3 R = reflect(-l, n);
	float cosAlpha = clamp(dot(E,R), 0, 1);
	
	color.rgb = MaterialAmbientColor + MaterialDiffuseColor * LightColor * LightPower * cosTheta;// / (distance*distance);// + MaterialSpecularColor * LightColor * LightPower * pow(cosAlpha, 5) / (distance*distance);
	color.a = 1;

	//color = texture(myTextureSampler, UV).rgb;
}