# README #

This little project is a space semi-game, which allows you to pilot a spaceship.

## Dependencies ##

* OpenGL 3.3
* GLEW
* GLFW

## Controls ##

* [W]/[S] to move forward/backward
* [A]/[D] to move left/right
* [Space]/[LShift] to move up/down
* [Q] and [E] to control roll
* [Up] and [Down] to control pitch
* [Left] and [Right] to control yaw
* [Esc] to exit

## Download ##

You can download Windows binary [here](https://bitbucket.org/Meghdor/opengl-demo/downloads/Demo.zip).
You may need to install [Visual C++ Redistributable for Visual Studio 2015 RC](https://www.microsoft.com/en-us/download/details.aspx?id=46881) before.
